(function() {
  'use strict';
  angular
    .module('front',[
        'mgcrea.ngStrap',
        'ngAnimate'
      ]
    )
    .controller('frontCtrl', function($scope, $aside) {
      // Show a basic aside from a controller
      $scope.aside = {
        "title": "LOGIN"
      };
    });
})();
