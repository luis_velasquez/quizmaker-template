var gulp   = require('gulp'),
    jshint = require('gulp-jshint'),
    browserSync = require('browser-sync').create(),
    inject = require('gulp-inject'),
    uglify  = require('gulp-uglify'),
    rename = require('gulp-rename'),
    series = require('stream-series'),
    concat  = require('gulp-concat');

gulp.task('default', ['watch', 'inject']);

gulp.task('concatenando', function () {
    gulp.src('./js/*.js')
        .pipe(concat('all.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('build/js'));
});

// we scan for files to inject
gulp.task('inject', function () {
  var target = gulp.src('index.html');
  // It's not necessary to read the files (will speed up things), we're only after their paths:
  var jquery = gulp.src(['js/jQuery/*.js'], {read: false});
  var sources = gulp.src(['js/*.js', 'css/*.css'], {read: false});
  var ngApp = gulp.src(['js/ng/**/*.js'], {read: false});
  var bootstrap = gulp.src(['css/vendor/**/*.css'], {read: false});

  return target.pipe(inject(series(bootstrap, jquery, sources, ngApp), {addRootSlash: false}))
    .pipe(gulp.dest('./'));
});

// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
  //livereload.listen();
  browserSync.init({
      server: {
          baseDir: "./"
      }
  });

  gulp.watch("*.html").on('change',  browserSync.reload);
  gulp.watch("js/partials/*.html").on('change',  browserSync.reload);
  gulp.watch("css/*.css", ['inject']).on('change', browserSync.reload);
  gulp.watch('js/**/*.js', ['jshint', 'inject']);
});

gulp.task('jshint', function() {
    return gulp.src('js/**/*.js')
           .pipe(jshint())
           .pipe(jshint.reporter('jshint-stylish'));
});
